FROM openjdk:8
EXPOSE 8090
WORKDIR /usr/app/
COPY /target/assignment*.jar /usr/app/
ENV SPRING_PROFILE_ACTIVE=h2
ENTRYPOINT java -jar -Dspring.profiles.active=${SPRING_PROFILE_ACTIVE} assignment*.jar