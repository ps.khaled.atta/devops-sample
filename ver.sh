date=$(git log -n 1  --format='%cd' --date=format:'%y%m%d')
tag=$(git rev-parse HEAD |  cut -c 1-8)
VERSION=$date-$tag
echo $VERSION
